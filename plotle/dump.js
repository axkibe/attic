/*
| Dumps a repository into a json file.
*/
'use strict';


// needs to be adapted to leveldb.
throw new Error( 'FIXME currently non functional' );


Error.stackTraceLimit = Infinity;

// Versions of dump files created
const dumpVersion = 1;

//const indent = ' ';
const indent = false;
global.CHECK = true;
global.NODE = true;


require( '@timberdoodle/tim' ).register( 'dump', module, 'src/tools/dump.js' );
require( '@timberdoodle/gleam' );

const config = require( '../config/intf' );
const fs = require( 'fs' );
const nano = require( 'nano' );
const { JsonFaucet } = require( 'async-json-stream' );
const Pouchdb = require( '../database/pouchdb' );

require( '../trace/base' ); // TODO working around cycle issues
//const change_list = require( '../change/list' );
//const change_wrap = require( '../change/wrap' );
const RefSpace = require( '../ref/space' );
const Repository = require( '../database/repository' );
//const user_info = require( '../user/info' );

/*
| Prints out usage info.
*/
const usage =
	function( )
{
	console.error( 'USAGE: node ' + module.filename + ' [FILENAME] [--overwrite]' );
};


/*
| The main runner.
*/
const run =
	async function( )
{
	let overwrite;
	let filename;
	require( '../../config' )( config.set );
	{
		const argv = process.argv;
		if( argv.length < 3 || argv.length > 4 ) { usage( ); return; }
		for( let a = 2; a < argv.length; a++ )
		{
			const arg = argv[ a ];
			if( arg === '--overwrite' )
			{
				if( overwrite ) { usage( ); return; }
				overwrite = true;
				continue;
			}

			if( arg !== '-' && arg[ 0 ] === '-' ) { usage( ); return; }
			if( filename ) { usage( ); return; }
			filename = arg;
		}
	}

	let pouchdb;
	if( config.get( 'database', 'pouchdb', 'enable' ) )
	{
		const port = config.get( 'database', 'pouchdb', 'port' );
		const host = config.get( 'database', 'pouchdb', 'host' );
		const dir = config.get( 'database', 'pouchdb', 'dir' );
		pouchdb = await Pouchdb.start( port, host, dir );
	}

	const faucet = new JsonFaucet( { indent : indent } );
	if( filename === '-' ) faucet.pipe( process.stdout );
	else
	{
		if( !overwrite )
		{
			let notthere;
			try { await fs.promises.access( filename, fs.constants.F_OK ); }
			catch( e ) { notthere = true; }

			if( !notthere )
			{
				console.error( 'Error: File exists and no overwrite requested.' );
				if( pouchdb ) pouchdb.shutdown( );
				return;
			}
		}

		faucet.pipe( fs.createWriteStream( filename ) );
	}

	await faucet.beginDocument( );
	await faucet.attribute( 'dbVersion', Repository.dbVersion  );
	await faucet.attribute( 'dumpVersion', dumpVersion );

	let url = config.get( 'database', 'url' );
	const name = config.get( 'database', 'name' );
	const passfile = config.get( 'database', 'passfile' );
	const builtUrl = await Repository.buildUrl( url, passfile );
	url = builtUrl.url;
	const connection = await nano( url );
	const db = await Repository.checkRepository( connection, name );

	if( db.error )
	{
		console.log( db );
		return -1;
	}

	// users
	{
		await faucet.attribute( 'users' );
		await faucet.beginObject( );
		const rows = await db.getUserNames( );
		for( let r of rows )
		{
			const ui = await db.getUser( r.key );
			await faucet.attribute( ui.name );
			await faucet.beginObject( );
			await faucet.attribute( 'news', ui.news );
			await faucet.attribute( 'mail', ui.mail );
			await faucet.attribute( 'passhash', ui.passhash );
			await faucet.endObject( );
		}
		await faucet.endObject( );
	}

	// spaces
	{
		const rows = await db.getSpaceIds( );
		await faucet.attribute( 'spaces' );
		await faucet.beginObject( );
		for( let r of rows )
		{
			const ref = RefSpace.createFromDbId( r.id );
			await faucet.attribute( ref.fullname );
			await faucet.beginArray( );

			const seqs = await db.getSpaceChangeSeqs( ref.dbChangesKey );
			let cs = 1;
			for( let s of seqs )
			{
				const key = s.key;
				if( key !== cs ) throw new Error( );
				const id = s.id;
				const o = await db.getChange( id );
				await faucet.beginObject( );
				await faucet.attribute( 'cid', o.cid );
				await faucet.attribute( 'date', o.date );
				await faucet.attribute( 'user', o.user );
				await faucet.attribute( 'changeList', o.changeList );
				await faucet.endObject( );
				cs++;
			}
			await faucet.endArray( );
		}
		await faucet.endObject( );
	}
	await faucet.endDocument( '\n' );

	if( pouchdb ) pouchdb.shutdown( );
};


run( );
