/*
| Starts the converter.
*/
Error.stackTraceLimit = Infinity;

const fromVersion = 46;

global.CHECK = true;
global.NODE = true;

const cname = 'Convert-' + fromVersion + '-to-' + ( fromVersion + 1 );

await import( 'ti2c' );
await import( 'ti2c-ot' );
await import( 'ti2c-gleam' );

const pkg =
	await ti2c.register(
		'name',    'plotle',
		'meta',    import.meta,
		'source',  'src/',
		'relPath', cname + '/Start',
		'codegen', 'codegen/'
	);
const Root = await pkg.import( cname + '/Root' );
await Root.init( fromVersion );
