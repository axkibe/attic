/*
| Starts the converter.
*/
'use strict';

Error.stackTraceLimit = Infinity;

const fromVersion = 40;

global.CHECK = true;
global.NODE = true;
global.VISUAL = true;

const cname = 'Convert-' + fromVersion + '-to-' + ( fromVersion + 1 );

const pkg =
	require( '@timberdoodle/tim' )
	.register( 'plotle', module, 'src/', cname + '/Start.js' );

// timberdoodle packages
require( '@timberdoodle/gleam' );
pkg.require( cname + '/Root' ).init( fromVersion );
