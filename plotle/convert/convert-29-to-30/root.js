/*
| Converts a v29 repository to v30.
|
| * spaces are keyed by their Uid instead of RefName.
*/
'use strict';


tim.define( module, ( def ) => {


if( TIM )
{
	def.attributes =
	{
		config : { type : 'protean' },

		fromVersion : { type : 'number' },

		toVersion : { type : 'number' },

		wet : { type : 'boolean' },
	};
}


const level = require( 'level' );
const util = require( 'util' );

tim.require( '../../trace/base' );
const ChangeList = tim.require( '../../change/list' );
const ChangeWrap = tim.require( '../../change/wrap' );
const ChangeSet = tim.require( '../../change/set' );
const FabricSpace = tim.require( '../../fabric/space' );
const FontRoot = tim.require( 'gleam/font/root' );
const ItemTwig = tim.require( '../../fabric/item/twig' );
const log = tim.require( '../../server/log' );
const RefSpace = tim.require( '../../ref/space' );
const Repository = tim.require( '../../database/repository' );
const TraceSpace = tim.require( '../../trace/space' );
const Uid = tim.require( '../../session/uid' );
const UserInfoSkid = tim.require( '../../database/userInfoSkid' );


/*
| Converts a space.
|
| ~srcRep: source repository
| ~trgRep: target repository
| ~spaceRef: space reference
| ~suid: space uid
*/
def.proto.convertSpace =
	async function( srcRep, trgRep, spaceRef, suid )
{
	log.log( '* converting space "' + spaceRef.fullname + '"' );

	// loads the space
	let seq = 1;
	let space = FabricSpace.create( 'items', ItemTwig.create( ) );

	for( ;; )
	{
		const changeSkid = await srcRep.getSpaceChange( spaceRef.fullname, seq );
		if( changeSkid === 'notFound' ) break;
		if( changeSkid.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = changeSkid.changeTree( space );
	}

	const changeSet =
		ChangeSet.create(
			'trace', TraceSpace.fakeRoot,
			'val', space,
			'prev', FabricSpace.create( 'items', ItemTwig.create( ) )
		);

	const cw =
		ChangeWrap.create(
			'cid', Uid.newUid( ),
			'changeList', ChangeList.Elements( changeSet ),
			'seq', 1
		);

	if( this.wet ) await trgRep.saveChange( cw, suid, ':convert', 1, Date.now( ) );
};


/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) log.log( '-- WET RUN! --' );
	else log.log( '-- dry run --' );

	{
		log.log( 'loading fonts' );
		const promise = util.promisify( FontRoot.load );
		await promise( 'DejaVuSans-Regular' );
	}

	const srcRep = await Repository.checkRepository( this.config.src, '' + this.fromVersion );
	if( srcRep === 'notFound' )
	{
		log.log( 'source repository not found!' );
		return;
	}

	let trgRep;
	if( this.wet )
	{
		const name = this.config.trg;
		{
			log.log( 'destroying possible preexisting target' );
			const db = level( name );
			await db.clear( );
			db.close( );
		}
		log.log( 'establishing target' );
		trgRep = await Repository.establishRepository( name, '' + this.toVersion, 'bare' );
	}

	{
		log.log( 'converting users' );
		const names = await srcRep.getUserNames( );
		for( let name of names )
		{
			let o = await srcRep._db.get( 'users:' + name );
			o = JSON.parse( o );
			const duis = UserInfoSkid.createFromJSON( o );
			if( this.wet ) await trgRep.saveUser( duis.asUser );
		}
	}

	{
		log.log( 'converting spaces' );
		const names = await srcRep.getSpaceNames( );
		for( let name of names )
		{
			const ref = RefSpace.FromDbName( name );
			let suid;
			if( this.wet ) suid = await trgRep.establishSpace( ref );
			await this.convertSpace( srcRep, trgRep, ref, suid );
		}
	}

	log.log( 'done' );
};


} );
