/*
| Converts a v32 repository to v33.
|
| * traces to item is prepended with trace to items.
*/
'use strict';


tim.define( module, ( def ) => {


if( TIM )
{
	def.attributes =
	{
		// converter config
		config: { type: 'protean' },

		// converting from this version
		fromVersion: { type: 'number' },

		// source database
		sdb: { type: [ 'undefined', 'protean' ] },

		// converting to this version
		toVersion: { type: 'number' },

		// target database
		tdb : { type: [ 'undefined', 'protean' ] },

		// dry or wet operation
		wet: { type: 'boolean' },
	};
}


const level = require( 'level' );
const lexi = require( 'lexicographic-integer' );
const util = require( 'util' );

tim.require( '../../trace/base' );
//const ChangeSkid = tim.require( '../../database/changeSkid' );
const FontRoot = tim.require( 'gleam/font/root' );


/*
| Converts a trace.
*/
def.proto.convertTrace =
	function( trace )
{
	console.log( 'T1', trace );
	// input trace
	const t1 = trace.trace;
	// output trace
	let t2 = trace.trace = [ ];

	for( let t of t1 )
	{
		if( t === 'item' ) t2.push( 'items' );
		t2.push( t );
	}
	console.log( 'T2', t2 );
	console.log( );
};


/*
| Converts an object.
*/
def.proto.convertObj =
	function( obj )
{
	if( obj.$type === 'trace' )
	{
		this.convertTrace( obj );
		return;
	}

	const keys = Object.keys( obj );
	for( let key of keys )
	{
		const val = obj[ key ];
		const type = typeof( val );

		if( type === 'array' )
		{
			for( let a = 0, alen = val.length; a < alen; a++ ) this.convertObj( val[ a ] );
		}
		if( type === 'object' ) this.convertObj( val );
	}
};


/*
| Converts a space.
|
| ~spaceName: space name
| ~spaceUid: space uid
*/
def.proto.convertSpace =
	async function( spaceName, spaceUid )
{
	// loads the space
	let seq = 1;
	for( ;; )
	{
		let obj;
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( seq, 'hex' );
		try { obj = await this.sdb.get( dbKey ); }
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}
		obj = JSON.parse( obj );
		this.convertObj( obj );

		console.inspect( obj );

		if( this.wet ) await this.tdb.put( dbKey, JSON.stringify( obj ) );
		seq++;
	}
};


def.proto.convertSpaces =
	async function( )

{
	console.log( 'converting spaces' );
	const spaceNames = await this.getSpaceNames( );
	for( let spaceName of spaceNames )
	{
		const spaceMeta = await this.sdb.get( spaceName );
		const smJson = JSON.parse( spaceMeta );
		console.log( 'doing', spaceName, '(' +  smJson.uid + ')' );
		if( this.wet ) await this.tdb.put( spaceName, spaceMeta );
		await this.convertSpace( spaceName, smJson.uid );
	}
};


def.proto.convertUsers =
	async function( )
{
	console.log( 'converting users' );
	const names = await this.getUserNames( );
	for( let name of names )
	{
		const dbKey = 'users:' + name;
		const dbVal = await this.sdb.get( dbKey );
		if( this.wet ) await this.tdb.put( dbKey, dbVal );
	}
};


/*
| Returns all space names.
*/
def.proto.getSpaceNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.sdb.createReadStream(
				{ gt: 'spaces:', lt: 'spaces;', values: false }
			)
			.on( 'data', ( data ) => names.push( data ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};


/*
| Returns all user names.
*/
def.proto.getUserNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.sdb.createReadStream( { gt: 'users:', lt: 'users;' } )
			.on( 'data', ( data ) => names.push( JSON.parse( data.value ).name ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};


/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) console.log( '-- WET RUN! --' );
	else console.log( '-- dry run --' );

	{
		console.log( 'loading fonts' );
		const promise = util.promisify( FontRoot.load );
		await promise( 'DejaVuSans-Regular' );
	}

	const sdb = level( this.config.src );
	let srcVersion;
	srcVersion = await sdb.get( 'version' );
	if( srcVersion.notFound )
	{
		console.log( 'source repository not found!' );
		return;
	}
	if( srcVersion !== '' + this.fromVersion ) throw new Error( 'wrong src version' );

	let tdb;
	if( this.wet )
	{
		tdb = level( this.config.trg );
		console.log( 'destroying possible preexisting target' );
		await tdb.clear( );
		console.log( 'establishing target' );
		await tdb.put( 'version', this.toVersion + '' );
	}

	let root = this.create( 'sdb', sdb, 'tdb', tdb );
	await root.convertUsers( );
	await root.convertSpaces( );
	console.log( 'done' );
};


} );
