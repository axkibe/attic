/*
| Converts a v25 repository to v26.
|
| * converts strokes j1Style to beginStyle
| * converts strokes j2Style to endStyle
| * for simplicity this again squashes history.
*/
'use strict';


tim.define( module, ( def ) => {


if( TIM )
{
	def.attributes =
	{
		config : { type : 'protean' },

		fromVersion : { type : 'number' },

		toVersion : { type : 'number' },

		wet : { type : 'boolean' },
	};
}


const nano = require( 'nano' );
const util = require( 'util' );

tim.require( '../../trace/base' );
const change_list = tim.require( '../../change/list' );
const change_wrap = tim.require( '../../change/wrap' );
const change_set = tim.require( '../../change/set' );
const database_pouchdb = tim.require( '../../database/pouchdb' );
const fabric_space = tim.require( '../../fabric/space' );
const fabric_stroke = tim.require( '../../fabric/stroke' );
const gleam_font_root = tim.require( 'gleam/font/root' );
const log = tim.require( '../../server/log' );
const ref_space = tim.require( '../../ref/space' );
const repository = tim.require( '../../database/repository' );
const session_uid = tim.require( '../../session/uid' );
const trace_space = tim.require( '../../trace/space' );
//const trace_item = tim.require( '../../trace/item' );


/*
| Creates a connection to source or target.
*/
def.proto.connect =
	async function(
		channel // 'src' or 'trg'
	)
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( channel ) !== 'string' ) throw new Error( );
/**/}

	const ccfg = this.config[ channel ];
	const builtUrl = await repository.buildUrl( ccfg.url, ccfg.passfile );
	const url = builtUrl.url;
	log.log( 'Connecting ' + channel + ' to ' + builtUrl.logUrl );
	return await nano( url );
};


/*
| Converts a space.
*/
def.proto.convertSpace =
	async function(
		srcRep,
		trgRep,
		spaceRef
	)
{
	log.log( '* converting space "' + spaceRef.fullname + '"' );
	const sRows = await srcRep.getSpaceChangeSeqs( spaceRef.dbChangesKey );

	// loads the space
	let seq = 1;
	let space = fabric_space.create( );
	for( let r of sRows )
	{
		const changeSkid = await srcRep.getChange( r.id );
		if( changeSkid.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = changeSkid.changeTree( space );
	}

	// changes relations to labels with strokes
	const keys = space.keys;
	for( let key of keys )
	{
		let item = space.get( key );
		if( item.timtype !== fabric_stroke ) continue;
		log.log( '+-* converting stroke ' + key );
		item =
			item.create(
				'j1Style', undefined,
				'j2Style', undefined,
				'beginStyle', item.j1Style,
				'endStyle', item.j2Style,
			);
		space = space.set( key, item );
	}

	const changeSet =
		change_set.create(
			'trace', trace_space.fakeRoot,
			'val', space,
			'prev', fabric_space.create( )
		);

	const cw =
		change_wrap.create(
			'cid', session_uid.newUid( ),
			'changeList', change_list.createWithElements( changeSet ),
			'seq', 1
		);

	if( this.wet ) await trgRep.saveChange( cw, spaceRef, ':convert', 1, Date.now( ) );
};


/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) log.log( '-- WET RUN! --' );
	else log.log( '-- dry run --' );

	{
		log.log( 'loading fonts' );
		const promise = util.promisify( gleam_font_root.load );
		await promise( 'DejaVuSans-Regular' );
	}

	let pouchdb;
	if( this.config.pouchdb.enable )
	{
		const pcfg = this.config.pouchdb;
		log.log( 'starting pouchdb ' + pcfg.host + ':' + pcfg.port + ' (' + pcfg.dir + ')' );
		pouchdb = await database_pouchdb.start( pcfg.port, pcfg.host, pcfg.dir );
	}

	const srcConnection = await this.connect( 'src' );
	const srcRep =
		await repository.checkRepository(
			srcConnection,
			this.config.src.name,
			this.fromVersion
		);

	if( srcRep.error === 'not_found' )
	{
		log.log( 'source repository not found!' );
		if( pouchdb ) pouchdb.shutdown( );
		return;
	}

	const trgConnection = await this.connect( 'trg' );

	let trgRep;
	if( this.wet )
	{
		const name = this.config.trg.name;
		log.log( 'destroying possible preexisting target' );
		try{ await trgConnection.db.destroy( name ); } catch( e ) { }
		log.log( 'establishing target' );
		trgRep =
			await repository.establishRepository(
				trgConnection,
				name,
				this.toVersion,
				'bare'
			);
	}

	{
		log.log( 'converting users' );
		const rows = await srcRep.getUserNames( );
		for( let r of rows )
		{
			const ui = await srcRep.getUser( r.key );
			if( this.wet ) await trgRep.saveUser( ui );
		}
	}

	{
		log.log( 'converting spaces' );
		const rows = await srcRep.getSpaceIds( );
		for( let r of rows )
		{
			const ref = ref_space.createFromDbId( r.id );
			if( this.wet ) await trgRep.establishSpace( ref );
			await this.convertSpace( srcRep, trgRep, ref );
		}
	}

	if( pouchdb ) pouchdb.shutdown( );
	log.log( 'done' );
};


} );
