/*
| Converts a v37 repository to v38.
|
| Changes portal traces from refSpace( key ) to refSpace.key as plain step
*/
'use strict';

if( TIM )
{
	def.attributes =
	{
		// converting from this version
		fromVersion: { type: 'number' },

		// source database
		srcDb: { type: [ 'undefined', 'protean' ] },

		// source dir
		srcDir: { type: [ 'undefined', 'string' ] },

		// converting to this version
		toVersion: { type: 'number' },

		// target database
		trgDb: { type: [ 'undefined', 'protean' ] },

		// source dir
		trgDir: { type: [ 'undefined', 'string' ] },

		// dry or wet operation
		wet: { type: 'boolean' },
	};
}

const level = require( 'level' );
const lexi = require( 'lexicographic-integer' );
//const util = require( 'util' );
//const FontRoot = tim.require( 'gleam/font/root' );

/*
| Converts an object.
*/
def.proto.convertObj =
	function( obj )
{
	const keys = Object.keys( obj );
	if( obj.$type === 'trace' )
	{
		const trace = obj.trace;
		for( let a = 0, alen = trace.length; a < alen; a++ )
		{
			if( trace[ a ] === '>refSpace' ) trace[ a ] = 'refSpace';
		}
		console.log( trace );
		return;
	}

	for( let key of keys )
	{
		const val = obj[ key ];
		const type = typeof( val );

		if( type === 'array' )
		{
			for( let a = 0, alen = val.length; a < alen; a++ ) this.convertObj( val[ a ] );
		}
		if( type === 'object' ) this.convertObj( val );
	}
};

/*
| Converts a space.
|
| ~spaceName: space name
| ~spaceUid: space uid
*/
def.proto.convertSpace =
	async function( spaceName, spaceUid )
{
	// loads the space
	let seq = 1;
	for( ;; )
	{
		let obj;
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( seq, 'hex' );
		try { obj = await this.srcDb.get( dbKey ); }
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}
		obj = JSON.parse( obj );
		this.convertObj( obj );
		if( this.wet ) await this.trgDb.put( dbKey, JSON.stringify( obj ) );
		seq++;
	}
};

def.proto.convertSpaces =
	async function( )
{
	console.log( 'converting spaces' );
	const spaceNames = await this.getSpaceNames( );
	for( let spaceName of spaceNames )
	{
		const spaceMeta = await this.srcDb.get( spaceName );
		const smJson = JSON.parse( spaceMeta );
		console.log( 'doing', spaceName, '(' +  smJson.uid + ')' );
		if( this.wet ) await this.trgDb.put( spaceName, spaceMeta );
		await this.convertSpace( spaceName, smJson.uid );
	}
};

def.proto.convertUsers =
	async function( )
{
	console.log( 'converting users' );
	const names = await this.getUserNames( );
	for( let name of names )
	{
		const dbKey = 'users:' + name;
		const dbVal = await this.srcDb.get( dbKey );
		if( this.wet ) await this.trgDb.put( dbKey, dbVal );
	}
};

/*
| Returns all space names.
*/
def.proto.getSpaceNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.srcDb.createReadStream(
				{ gt: 'spaces:', lt: 'spaces;', values: false }
			)
			.on( 'data', ( data ) => names.push( data ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};

/*
| Returns all user names.
*/
def.proto.getUserNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.srcDb.createReadStream( { gt: 'users:', lt: 'users;' } )
			.on( 'data', ( data ) => names.push( JSON.parse( data.value ).name ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};

/*
| Initializes the converter.
*/
def.static.init =
	function( fromVersion )
{
	if( process.argv.length !== 3 ) { Self.usage( ); return; }
	const arg = process.argv[ 2 ];

	const toVersion = fromVersion + 1;
	const srcDir = '/home/axel/plotle-' + fromVersion;
	const trgDir = '/home/axel/plotle-' + toVersion;

	// if false doesn't do anything to the target.
	let wet;
	switch( arg )
	{
		case 'dry': wet = false; break;
		case 'wet': wet = true; break;
		default: Self.usage( ); return;
	}

	Self.create(
		'fromVersion', fromVersion,
		'srcDir', srcDir,
		'toVersion', toVersion,
		'trgDir', trgDir,
		'wet', wet
	).run( );
};

/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) console.log( '-- WET RUN! --' );
	else console.log( '-- dry run --' );

	/*
	{
		console.log( 'loading fonts' );
		const promise = util.promisify( FontRoot.load );
		await promise( 'DejaVuSans-Regular' );
	}
	*/

	const srcDb = level( this.srcDir );
	const srcVersion = await srcDb.get( 'version' );
	if( srcVersion.notFound )
	{
		console.log( 'source repository not found!' );
		return;
	}
	if( srcVersion !== '' + this.fromVersion ) throw new Error( 'wrong src version' );

	let trgDb;
	if( this.wet )
	{
		trgDb = level( this.trgDir );
		console.log( 'destroying possible preexisting target' );
		await trgDb.clear( );
		console.log( 'establishing target' );
		await trgDb.put( 'version', this.toVersion + '' );
	}

	let root = this.create( 'srcDb', srcDb, 'trgDb', trgDb );
	await root.convertUsers( );
	await root.convertSpaces( );
	console.log( 'done' );
};

/*
| Prints out usage info.
*/
def.static.usage =
	function( )
{
	console.log( 'USAGE: node ' + module.filename + ' [dry or wet]' );
};
