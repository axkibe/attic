/*
| Converts a v25 repository to v26.
|
| * converts strokes j1Style to beginStyle
| * converts strokes j2Style to endStyle
| * for simplicity this again squashes history.
*/
'use strict';


tim.define( module, ( def ) => {


if( TIM )
{
	def.attributes =
	{
		config : { type : 'protean' },

		fromVersion : { type : 'number' },

		toVersion : { type : 'number' },

		wet : { type : 'boolean' },
	};
}


const nano = require( 'nano' );
const util = require( 'util' );

tim.require( '../../trace/base' );
const ChangeList = tim.require( '../../change/list' );
const ChangeWrap = tim.require( '../../change/wrap' );
const ChangeSet = tim.require( '../../change/set' );
const ChangeSkid = tim.require( '../../database/changeSkid' );
const FabricSpace = tim.require( '../../fabric/space' );
//const FabricStroke = tim.require( '../../fabric/stroke' );
const FontRoot = tim.require( 'gleam/font/root' );
const ItemTwig = tim.require( '../../fabric/item/twig' );
const log = tim.require( '../../server/log' );
const Pouchdb = tim.require( '../../database/pouchdb' );
const RefSpace = tim.require( '../../ref/space' );
const Repository = tim.require( '../../database/repository' );
//const TraceItem = tim.require( '../../trace/item' );
const TraceSpace = tim.require( '../../trace/space' );
const Uid = tim.require( '../../session/uid' );
const UserInfoSkid = tim.require( '../../database/userInfoSkid' );


/*
| Creates a connection to source or target.
|
| ~channel: 'src' or 'trg'
*/
def.proto.connect =
	async function( channel )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( channel ) !== 'string' ) throw new Error( );
/**/}

	const ccfg = this.config[ channel ];
	const builtUrl = await Repository.buildUrl( ccfg.url, ccfg.passfile );
	const url = builtUrl.url;
	log.log( 'Connecting ' + channel + ' to ' + builtUrl.logUrl );
	return await nano( url );
};


/*
| Converts an object.
*/
def.proto.convertObj =
	function( obj )
{
	for( let key in obj )
	{
		const val = obj[ key ];
		const type = typeof( val );

		if( key === 'type' )
		{
			switch( val )
			{
				case 'ShapeList' : obj.type = 'FigureList'; break;
			}
		}

		if( type === 'object' ) obj[ key ] = this.convertObj( val );
		if( type === 'array' )
		{
			for( let a = 0, alen = val.length; a < alen; a++ )
			{
				val[ a ] = this.convertObj( val[ a ] );
			}
		}
	}
	return obj;
};


/*
| Converts a space.
*/
def.proto.convertSpace =
	async function( srcRep, trgRep, spaceRef )
{
	log.log( '* converting space "' + spaceRef.fullname + '"' );
	const sRows = await srcRep.getSpaceChangeSeqs( spaceRef.dbChangesKey );

	// loads the space
	let seq = 1;
	let space = FabricSpace.create( 'items', ItemTwig.create( ) );
	for( let r of sRows )
	{
		let o = await srcRep._db.get( r.id );
		o = this.convertObj( o );
//		console.inspect( 'XXX', o );
		const cs = ChangeSkid.createFromJSON( o );
		if( cs.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = cs.changeTree( space );
	}

	const changeSet =
		ChangeSet.create(
			'trace', TraceSpace.fakeRoot,
			'val', space,
			'prev', FabricSpace.create( 'items', ItemTwig.create( ) )
		);

	const cw =
		ChangeWrap.create(
			'cid', Uid.newUid( ),
			'changeList', ChangeList.Elements( changeSet ),
			'seq', 1
		);

	if( this.wet ) await trgRep.saveChange( cw, spaceRef, ':convert', 1, Date.now( ) );
};


/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) log.log( '-- WET RUN! --' );
	else log.log( '-- dry run --' );

	{
		log.log( 'loading fonts' );
		const promise = util.promisify( FontRoot.load );
		await promise( 'DejaVuSans-Regular' );
	}

	let pouchdb;
	if( this.config.pouchdb.enable )
	{
		const pcfg = this.config.pouchdb;
		log.log( 'starting pouchdb ' + pcfg.host + ':' + pcfg.port + ' (' + pcfg.dir + ')' );
		pouchdb = await Pouchdb.start( pcfg.port, pcfg.host, pcfg.dir );
	}

	const srcConnection = await this.connect( 'src' );
	const srcRep =
		await Repository.checkRepository(
			srcConnection,
			this.config.src.name,
			this.fromVersion
		);

	if( srcRep.error === 'not_found' )
	{
		log.log( 'source repository not found!' );
		if( pouchdb ) pouchdb.shutdown( );
		return;
	}

	const trgConnection = await this.connect( 'trg' );

	let trgRep;
	if( this.wet )
	{
		const name = this.config.trg.name;
		log.log( 'destroying possible preexisting target' );
		try{ await trgConnection.db.destroy( name ); } catch( e ) { }
		log.log( 'establishing target' );
		trgRep =
			await Repository.establishRepository( trgConnection, name, this.toVersion, 'bare' );
	}

	{
		log.log( 'converting users' );
		const rows = await srcRep.getUserNames( );
		for( let r of rows )
		{
//			const ui = await srcRep.getUser( r.key );
			const o = await srcRep._db.get( 'users:' + r.key );
			o.type = 'UserInfoSkid';
			const duis = UserInfoSkid.createFromJSON( o );
			if( this.wet ) await trgRep.saveUser( duis.asUser );
		}
	}

	{
		log.log( 'converting spaces' );
		const rows = await srcRep.getSpaceIds( );
		for( let r of rows )
		{
			const ref = RefSpace.createFromDbId( r.id );
			if( this.wet ) await trgRep.establishSpace( ref );
			await this.convertSpace( srcRep, trgRep, ref );
		}
	}

	if( pouchdb ) pouchdb.shutdown( );
	log.log( 'done' );
};


} );
