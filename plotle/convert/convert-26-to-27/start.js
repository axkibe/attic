/*
| Starts the converter.
*/
'use strict';


Error.stackTraceLimit = Infinity;
process.on( 'unhandledRejection', err => { throw err; } );

const fromVersion = 26;
const toVersion = fromVersion + 1;
const config =
{
	pouchdb :
	{
		//enable : true,
		enable : false,
		dir : '/home/axel/repository/',
		host : 'localhost',
		port : 5984,
	},
	src :
	{
		name : 'plotle-' + fromVersion,
		passfile : './dbadminpass',
		url : 'http://admin:PASSWORD@127.0.0.1:5984',
	},
	trg :
	{
		name : 'plotle-' + toVersion,
		passfile : './dbadminpass',
		url : 'http://admin:PASSWORD@127.0.0.1:5984',
	},
};

global.CHECK = true;
global.NODE = true;
global.VISUAL = true;

// if false doesn't do anything to the target.
let wet = false;

const cname = 'convert-' + fromVersion + '-to-' + toVersion;
require( '@timberdoodle/tim' )
.register( 'plotle', module, 'src/tools/' + cname + '/start.js' );

// timberdoodle packages
require( '@timberdoodle/gleam' );
const convert_root = require( './root' );


/*
| Prints out usage info.
*/
const usage =
	function( )
{
	console.log( 'USAGE: node ' + module.filename + ' [dry or wet]' );
};

if( process.argv.length !== 3 ) { usage( ); return; }
const arg = process.argv[ 2 ];

switch( arg )
{
	case 'dry' : wet = false; break;
	case 'wet' : wet = true; break;
	default : usage( ); return;
}

convert_root.create(
	'config', config,
	'fromVersion', fromVersion,
	'toVersion', toVersion,
	'wet', wet
).run( );
