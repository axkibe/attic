/*
| Converts a v47 repository to v48.
|
| Fixes the from to undefined on the initial set.
*/
def.abstract = true;

import { default as level } from 'level';
import { default as lexi  } from 'lexicographic-integer';

import { Self as ChangeTreeSet } from '{ot:Change/Tree/Set}';
import { Self as ChangeSkid    } from '{Server/Database/ChangeSkid/Self}';
import { Self as ChangeWrap    } from '{ot:Change/Wrap}';
import { Self as ItemTwig      } from '{Fabric/Item/Twig}';
import { Self as Plan          } from '{Trace/Plan}';
import { Self as Space         } from '{Fabric/Space}';
import { Self as TraceRoot     } from '{Trace/Root}';
import { Self as Sequence      } from '{ot:Change/Sequence}';
import { Self as Uid           } from '{ot:Uid}';

/*
| Converts an object.
*/
function convertObj( obj )
{
	const keys = Object.keys( obj );
	for( let key of keys )
	{
		const val = obj[ key ];
		const type = typeof( val );
		switch( type )
		{
			case 'array':
				for( let a = 0, alen = val.length; a < alen; a++ )
				{
					convertObj( val[ a ] );
				}
				break;

			case 'object':
				convertObj( val );
				break;
		}
	}
}

/*
| Converts a space.
|
| ~wet:       false if dry run
| ~srcDb:     source database
| ~trgDb:     target database
| ~spaceName: name of space to convert
| ~spaceUid:  uid of space to convert
*/
async function convertSpace( wet, srcDb, trgDb, spaceName, spaceUid )
{
/**/if( CHECK && arguments.length !== 5 ) throw new Error( );

	console.log( '* converting space "' + spaceName + '"' + '(' + spaceUid + ')' );

	// loads the space
	let seqNr = 1;
	let space = Space.create( 'items', ItemTwig.create( ) );

	console.log( '** replaying history' );

	for(;;)
	{
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( seqNr, 'hex' );

		let obj;
		try
		{
			obj = await srcDb.get( dbKey );
		}
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}

		obj = JSON.parse( obj );
		convertObj( obj );

		const changeSkid = ChangeSkid.FromJson( obj, Plan.space );
		seqNr++;
		space = changeSkid.changeTree( space );
	}

	{
		console.log( '** converting json' );
		const spj = JSON.parse( space.jsonfy( '' ) );
		convertObj( spj );
		space = Space.FromJson( spj, Plan.space );
	}

	{
		if( !space.design )
		{
			space = space.create( 'design', 'GrugaDark' );
		}
	}

	console.log( '** writing as new space' );
	{
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( 1, 'hex' );

		const changeSet =
			ChangeTreeSet.create(
				'trace', TraceRoot.space,
				'val', space,
				'prev', undefined,
			);

		const cw =
			ChangeWrap.create(
				'id', Uid.newUid( ),
				'changes', Sequence.Elements( changeSet ),
			);

		const cs = ChangeSkid.ChangeWrap( cw, ':convert', Date.now( ) );

		if( wet )
		{
			await trgDb.put( dbKey, cs.jsonfy( ) );
		}
	}
}

/*
| Converts all spaces.
|
| ~wet:   if false dry run
| ~srcDb: source database
| ~trgDb: target database
*/
async function convertSpaces( wet, srcDb, trgDb )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	console.log( 'converting spaces' );

	const spaceNames = await getSpaceNames( srcDb );

	for( let spaceName of spaceNames )
	{
		const spaceMeta = await srcDb.get( spaceName );
		const smJson = JSON.parse( spaceMeta );

		console.log( 'doing', spaceName, '(' +  smJson.uid + ')' );

		if( wet )
		{
			await trgDb.put( spaceName, spaceMeta );
		}

		await convertSpace( wet, srcDb, trgDb, spaceName, smJson.uid );
	}
}

/*
| Converts all users.
|
| ~wet:   false if dry run
| ~srcDb: source database
| ~trgDb: target database
*/
async function convertUsers( wet, srcDb, trgDb )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	console.log( 'converting users' );

	const names = await getUserNames( srcDb );

	for( let name of names )
	{
		{
			// copies user info
			const dbKey = 'users:' + name;
			const dbVal = await srcDb.get( dbKey );
			if( wet )
			{
				await trgDb.put( dbKey, dbVal );
			}
		}

		{
			// copies seen info
			const dbKey = 'seen:' + name;
			let dbVal;
			try { dbVal = await srcDb.get( dbKey ); }
			catch( e ) { /* ignore */ }

			if( wet && dbVal )
			{
				await trgDb.put( dbKey, dbVal );
			}
		}
	}
}

/*
| Returns all space names.
*/
async function getSpaceNames( srcDb )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const names = [ ];
	const it = srcDb.iterator( { gt: 'spaces:', lt: 'spaces;' } );
	for await ( const [ key ] of it )
	{
		names.push( key );
	}
	return names;
}

/*
| Returns all user names.
*/
async function getUserNames( srcDb )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const names = [ ];
	const it = srcDb.iterator( { gt: 'users:', lt: 'users;' } );
	for await ( const [ , value ] of it )
	{
		const o = JSON.parse( value );
		names.push( o.name );
	}
	return names;
}

/*
| The main runner.
|
| ~wet:         if false dry run
| ~srcDir:      source directory
| ~trgDir:      target direcotry
| ~fromVersion: version number to convert from
| ~toVersion:   version number to convert to
*/
async function run( wet, srcDir, trgDir, fromVersion, toVersion )
{
/**/if( CHECK && arguments.length !== 5 ) throw new Error( );

	if( wet )
	{
		console.log( '-- WET RUN! --' );
	}
	else
	{
		console.log( '-- dry run --' );
	}

	//{
	//	console.log( 'loading fonts' );
	//	const promise = util.promisify( FontRoot.load );
	//	await promise( 'DejaVuSans-Regular' );
	//}

	const srcDb = new level.Level( srcDir );
	await srcDb.open( );

	const srcVersion = await srcDb.get( 'version' );
	if( srcVersion.notFound )
	{
		console.log( 'source repository not found!' );
		return;
	}

	if( srcVersion !== '' + fromVersion )
	{
		throw new Error( 'wrong src version' );
	}

	let trgDb;
	if( wet )
	{
		trgDb = new level.Level( trgDir );
		await trgDb.open( );
		console.log( 'destroying possible preexisting target' );
		await trgDb.clear( );
		console.log( 'establishing target' );
		await trgDb.put( 'version', toVersion + '' );
	}

	await convertUsers( wet, srcDb, trgDb );
	await convertSpaces( wet, srcDb, trgDb );

	console.log( 'done' );
}

/*
| Prints out usage info.
*/
function usage( )
{
	console.log( 'USAGE: node ' + process.argv[ 1 ] + ' [dry or wet]' );
}

/*
| Initializes the converter.
*/
def.static.init =
	async function( fromVersion )
{
	if( process.argv.length !== 3 )
	{
		usage( );
		return;
	}
	const arg = process.argv[ 2 ];

	const toVersion = fromVersion + 1;
	const srcDir = '/home/axel/plotle-' + fromVersion;
	const trgDir = '/home/axel/plotle-' + toVersion;

	let wet;

	// if false doesn't do anything to the target.
	switch( arg )
	{
		case 'dry':
			wet = false;
			break;

		case 'wet':
			wet = true;
			break;

		default:
			Self.usage( );
			return;
	}

	await run( wet, srcDir, trgDir, fromVersion, toVersion );
};
