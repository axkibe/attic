/*
| Converts a v23 repository to v24.
|
| * replaces the jp1/jp2 ancillaries of strokes with shape1/shape2 ancillaries.
| * for simplicity this again squashes history.
*/
'use strict';


Error.stackTraceLimit = Infinity;
process.on( 'unhandledRejection', err => { throw err; } );


// if false doesn't do anything to the target.
let wet = false;

const fromVersion = 23;
const toVersion = fromVersion + 1;

const config =
{
	pouchdb :
	{
		enable : false,
		dir : '/home/axel/repository/',
		host : 'localhost',
		port : 8834,
	},
	src :
	{
		name : 'plotle-' + fromVersion,
		passfile : './dbadminpass',
		url : 'http://admin:PASSWORD@127.0.0.1:5984',
	},
	trg :
	{
		name : 'plotle-' + toVersion,
		passfile : './dbadminpass',
		url : 'http://admin:PASSWORD@127.0.0.1:5984',
	},
};

global.CHECK = true;
global.NODE = true;
global.VISUAL = true;

// registers with tim.js
{
	require( 'tim.js' );
	const ending = 'src/tools/convert-' + fromVersion + '-to-' + toVersion + '.js';
	const filename = module.filename;
	if( !filename.endsWith( ending ) ) throw new Error( );
	const rootPath = filename.substr( 0, filename.length - ending.length );
	const timcodePath = rootPath.substr( 0, rootPath.lastIndexOf( '/' ) ) + '/timcode/';
	tim.catalog.addRootDir( rootPath, 'convert', timcodePath );
}


const nano = require( 'nano' );
const util = require( 'util' );

require( '../trace/base' ); // TODO working around cycle issues
const change_list = require( '../change/list' );
const change_wrap = require( '../change/wrap' );
const change_set = require( '../change/set' );
const fabric_space = require( '../fabric/space' );
const fabric_stroke = require( '../fabric/stroke' );
const gleam_font_root = require( '../gleam/font/root' );
const database_pouchdb = require( '../database/pouchdb' );
const log = require( '../server/log' );
const ref_space = require( '../ref/space' );
const repository = require( '../database/repository' );
const session_uid = require( '../session/uid' );
//const trace_root = require( '../trace/root' );
const trace_space = require( '../trace/space' );
const trace_item = require( '../trace/item' );


/*
| Creates a connection to source or target.
*/
const connect =
	async function(
		channel // 'src' or 'trg'
	)
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( channel ) !== 'string' ) throw new Error( );
/**/}

	const ccfg = config[ channel ];
	const builtUrl = await repository.buildUrl( ccfg.url, ccfg.passfile );
	const url = builtUrl.url;
	log.log( 'Connecting ' + channel + ' to ' + builtUrl.logUrl );
	return await nano( url );
};


/*
| Prints out usage info.
*/
const usage =
	function( )
{
	console.log( 'USAGE: node ' + module.filename + ' [dry or wet]' );
};


/*
| Converts a space.
*/
const convertSpace =
	async function(
		srcRep,
		trgRep,
		spaceRef
	)
{
	log.log( '* converting space "' + spaceRef.fullname + '"' );

	const sRows = await srcRep.getSpaceChangeSeqs( spaceRef.dbChangesKey );

	// loads the space
	let seq = 1;
	let space = fabric_space.create( );
	for( let r of sRows )
	{
		const changeSkid = await srcRep.getChange( r.id );
		if( changeSkid.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = changeSkid.changeTree( space );
	}

	// changes relations to labels with strokes
	const keys = space.keys;
	//const ts = trace_root.singleton.appendSpace;
	for( let key of keys )
	{
		let item = space.get( key );
		//const rank = space.rankOf( key );

		if( item.timtype !== fabric_stroke ) continue;

		log.log( '+-* converting stroke ' + key );

		item =
			item.create(
				'j1Style', item.js1,
				'j2Style', item.js2,
				'jp1', undefined,
				'jp2', undefined,
				'js1', undefined,
				'js2', undefined
			);

		if( item.j1.timtype === trace_item )
		{
			let t1 = item.j1;
			if( t1.traceRoot ) t1 = t1.chopRoot;
			const i1 = t1.pick( space );
			const s1 = i1.shape;

			item = item.create( 'j1', t1, 'j1Shape', s1 );
		}

		if( item.j2.timtype === trace_item )
		{
			let t2 = item.j2;
			if( t2.traceRoot ) t2 = t2.chopRoot;
			const i2 = t2.pick( space );
			const s2 = i2.shape;

			item = item.create( 'j2', t2, 'j2Shape', s2 );
		}

		space = space.set( key, item );
	}

	const changeSet =
		change_set.create(
			'trace', trace_space.fakeRoot,
			'val', space,
			'prev', fabric_space.create( )
		);

	const cw =
		change_wrap.create(
			'cid', session_uid.newUid( ),
			'changeList', change_list.createWithElements( changeSet ),
			'seq', 1
		);

	if( wet ) await trgRep.saveChange( cw, spaceRef, ':convert', 1, Date.now( ) );
};


/*
| The main runner.
*/
const run =
	async function( )
{
	if( process.argv.length !== 3 ) { usage( ); return; }

	const arg = process.argv[ 2 ];

	switch( arg )
	{
		case 'dry' : wet = false; break;
		case 'wet' : wet = true; break;
		default : usage( ); return;
	}

	if( wet ) log.log( '-- WET RUN! --' );
	else log.log( '-- dry run --' );

	{
		log.log( 'loading fonts' );
		const promise = util.promisify( gleam_font_root.load );
		await promise( 'DejaVuSans-Regular' );
	}

	let pouchdb;
	if( config.pouchdb.enable )
	{
		const pcfg = config.pouchdb;
		log.log( 'starting pouchdb ' + pcfg.host + ':' + pcfg.port + ' (' + pcfg.dir + ')' );
		pouchdb = await database_pouchdb.start( pcfg.port, pcfg.host, pcfg.dir );
	}

	const srcConnection = await connect( 'src' );
	const srcRep =
		await repository.checkRepository(
			srcConnection,
			config.src.name,
			fromVersion
		);

	if( srcRep.error === 'not_found' )
	{
		log.log( 'source repository not found!' );
		if( pouchdb ) pouchdb.shutdown( );
		return;
	}

	const trgConnection = await connect( 'trg' );

	let trgRep;
	if( wet )
	{
		const name = config.trg.name;

		log.log( 'destroying possible preexisting target' );
		try{ await trgConnection.db.destroy( name ); } catch( e ) { }

		log.log( 'establishing target' );
		trgRep =
			await repository.establishRepository(
				trgConnection,
				name,
				toVersion,
				'bare'
			);
	}

	{
		log.log( 'converting users' );
		const rows = await srcRep.getUserNames( );
		for( let r of rows )
		{
			const ui = await srcRep.getUser( r.key );
			if( wet ) await trgRep.saveUser( ui );
		}
	}

	{
		log.log( 'converting spaces' );
		const rows = await srcRep.getSpaceIds( );
		for( let r of rows )
		{
			const ref = ref_space.createFromDbId( r.id );
			if( wet ) await trgRep.establishSpace( ref );
			await convertSpace( srcRep, trgRep, ref );
		}
	}

	if( pouchdb ) pouchdb.shutdown( );
	log.log( 'done' );
};


run( );
