/*
| Converts a v22 repository to v23.
|
| This replaces relations with a label and two strokes.
| For simplicity this squashes history.
*/
'use strict';


Error.stackTraceLimit = Infinity;
process.on( 'unhandledRejection', err => { throw err; } );


/*
| If false doesn't do anything to the target.
*/
let wet = false;

const fromVersion = 22;
const toVersion = fromVersion + 1;

const config =
{
	pouchdb :
	{
		enable : true,
		dir : '/home/axel/repository/',
		host : 'localhost',
		port : 8834,
	},
	src :
	{
		name : 'plotle-' + fromVersion,
		passfile : './dbadminpass',
		url : 'http://admin:PASSWORD@127.0.0.1:8834',
	},
	trg :
	{
		name : 'plotle-' + toVersion,
		passfile : './dbadminpass',
		url : 'http://admin:PASSWORD@127.0.0.1:8834',
	},
};

global.CHECK = true;
global.NODE = true;
global.VISUAL = true;

// registers with tim.js
{
	require( 'tim.js' );
	const ending = 'src/tools/convert-' + fromVersion + '-to-' + toVersion + '.js';
	const filename = module.filename;
	if( !filename.endsWith( ending ) ) throw new Error( );
	const rootPath = filename.substr( 0, filename.length - ending.length );
	const timcodePath = rootPath.substr( 0, rootPath.lastIndexOf( '/' ) ) + '/timcode/';
	tim.catalog.addRootDir( rootPath, 'convert', timcodePath );
}


const nano = require( 'nano' );
const util = require( 'util' );

require( '../trace/base' ); // TODO working around cycle issues
const change_list = require( '../change/list' );
const change_wrap = require( '../change/wrap' );
const change_set = require( '../change/set' );
const fabric_label = require( '../fabric/label' );
const fabric_relation = require( '../fabric/relation' );
const fabric_space = require( '../fabric/space' );
const fabric_stroke = require( '../fabric/stroke' );
const gleam_font_root = require( '../gleam/font/root' );
const database_pouchdb = require( '../database/pouchdb' );
const log = require( '../server/log' );
const ref_space = require( '../ref/space' );
const repository = require( '../database/repository' );
const session_uid = require( '../session/uid' );
const trace_root = require( '../trace/root' );
const trace_space = require( '../trace/space' );


/*
| Creates a connection to source or target.
*/
const connect =
	async function(
		channel // 'src' or 'trg'
	)
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( channel ) !== 'string' ) throw new Error( );
/**/}

	const ccfg = config[ channel ];
	const builtUrl = await repository.buildUrl( ccfg.url, ccfg.passfile );
	const url = builtUrl.url;
	log.log( 'Connecting ' + channel + ' to ' + builtUrl.logUrl );
	return await nano( url );
};


/*
| Prints out usage info.
*/
const usage =
	function( )
{
	console.log( 'USAGE: node ' + module.filename + ' [dry or wet]' );
};


/*
| Converts a space.
*/
const convertSpace =
	async function(
		srcRep,
		trgRep,
		spaceRef
	)
{
	log.log( 'converting space "' + spaceRef.fullname + '"' );

	const sRows = await srcRep.getSpaceChangeSeqs( spaceRef.dbChangesKey );

	// loads the space
	let seq = 1;
	let space = fabric_space.create( );
	for( let r of sRows )
	{
		const changeSkid = await srcRep.getChange( r.id );
		if( changeSkid.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = changeSkid.changeTree( space );
	}

	// changes relations to labels with strokes
	const keys = space.keys;
	const ts = trace_root.singleton.appendSpace;
	for( let key of keys )
	{
		const item = space.get( key );
		const rank = space.rankOf( key );

		if( item.timtype !== fabric_relation ) continue;

		log.log( 'converting relation ' + key );

		const label =
			fabric_label.create(
				'doc', item.doc,
				'fontsize', item.fontsize,
				'zone', item.zone,
				'trace', ts.appendItem( key )
			);

		const lineTrace = ts.appendItem( session_uid.newUid( ) );
		const line =
			fabric_stroke.create(
				'j1', ts.appendItem( item.item1key ),
				'j2', ts.appendItem( key ),
				'js1', 'none',
				'js2', 'none',
				'trace', lineTrace
			);

		const arrowTrace = ts.appendItem( session_uid.newUid( ) );
		const arrow =
			fabric_stroke.create(
				'j1', ts.appendItem( key ),
				'j2', ts.appendItem( item.item2key ),
				'js1', 'none',
				'js2', 'arrow',
				'trace', arrowTrace
			);

		const item1 = space.get( item.item1key );
		const item2 = space.get( item.item2key );

		space = space.set( key, label );
		space = space.create( 'twig:insert', arrowTrace.key, rank + 1, arrow );
		space = space.create( 'twig:insert', lineTrace.key, rank + 1, line );

		space = line.ancillaryByJPS( item1.shape, item.shape ).changeTree( space );
		space = arrow.ancillaryByJPS( item.shape, item2.shape ).changeTree( space );
	}

	const changeSet =
		change_set.create(
			'trace', trace_space.fakeRoot,
			'val', space,
			'prev', fabric_space.create( )
		);

	const cw =
		change_wrap.create(
			'cid', session_uid.newUid( ),
			'changeList', change_list.createWithElements( changeSet ),
			'seq', 1
		);

	if( wet ) await trgRep.saveChange( cw, spaceRef, ':convert', 1, Date.now( ) );
};


/*
| The main runner.
*/
const run =
	async function( )
{
	if( process.argv.length !== 3 ) { usage( ); return; }

	const arg = process.argv[ 2 ];

	switch( arg )
	{
		case 'dry' : wet = false; break;
		case 'wet' : wet = true; break;
		default : usage( ); return;
	}

	if( wet ) log.log( '-- WET RUN! --' );
	else log.log( '-- dry run --' );

	{
		log.log( 'loading fonts' );
		const promise = util.promisify( gleam_font_root.load );
		await promise( 'DejaVuSans-Regular' );
	}

	let pouchdb;
	if( config.pouchdb.enable )
	{
		const pcfg = config.pouchdb;
		log.log( 'starting pouchdb ' + pcfg.host + ':' + pcfg.port + ' (' + pcfg.dir + ')' );
		pouchdb = await database_pouchdb.start( pcfg.port, pcfg.host, pcfg.dir );
	}

	const srcConnection = await connect( 'src' );
	const srcRep =
		await repository.checkRepository(
			srcConnection,
			config.src.name,
			fromVersion
		);

	if( srcRep.error === 'not_found' )
	{
		log.log( 'source repository not found!' );
		if( pouchdb ) pouchdb.shutdown( );
		return;
	}

	const trgConnection = await connect( 'trg' );

	let trgRep;
	if( wet )
	{
		const name = config.trg.name;

		log.log( 'destroying possible preexisting target' );
		try{ await trgConnection.db.destroy( name ); } catch( e ) { }

		log.log( 'establishing target' );
		trgRep =
			await repository.establishRepository(
				trgConnection,
				name,
				toVersion,
				'bare'
			);
	}

	{
		log.log( 'converting users' );
		const rows = await srcRep.getUserNames( );
		for( let r of rows )
		{
			const ui = await srcRep.getUser( r.key );
			if( wet ) await trgRep.saveUser( ui );
		}
	}

	{
		log.log( 'converting spaces' );
		const rows = await srcRep.getSpaceIds( );
		for( let r of rows )
		{
			const ref = ref_space.createFromDbId( r.id );
			if( wet ) await trgRep.establishSpace( ref );
			await convertSpace( srcRep, trgRep, ref );
		}
	}

	if( pouchdb ) pouchdb.shutdown( );
	log.log( 'done' );
};


run( );
