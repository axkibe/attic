/*
| Converts a v33 repository to v34.
|
| * combined trace parts are separated.
*/
'use strict';


tim.define( module, ( def ) => {


if( TIM )
{
	def.attributes =
	{
		// converter config
		config: { type: 'protean' },

		// converting from this version
		fromVersion: { type: 'number' },

		// source database
		sdb: { type: [ 'undefined', 'protean' ] },

		// converting to this version
		toVersion: { type: 'number' },

		// target database
		tdb : { type: [ 'undefined', 'protean' ] },

		// dry or wet operation
		wet: { type: 'boolean' },
	};
}


const level = require( 'level' );
const lexi = require( 'lexicographic-integer' );
const util = require( 'util' );

tim.require( '../../trace/base' );
const ChangeList = tim.require( '../../change/list' );
const ChangeSet = tim.require( '../../change/set' );
const ChangeSkid = tim.require( '../../database/changeSkid' );
const ChangeWrap = tim.require( '../../change/wrap' );
const FabricPortal = tim.require( '../../fabric/portal' );
const FabricSpace = tim.require( '../../fabric/space' );
const FontRoot = tim.require( 'gleam/font/root' );
const ItemTwig = tim.require( '../../fabric/item/twig' );
const Plan = tim.require( '../../trace/plan' );
const RefSpace = tim.require( '../../ref/space' );
const TraceRoot = tim.require( '../../trace/root' );
const Uid = tim.require( '../../session/uid' );


/*
| Converts a trace.
*/
/*
def.proto.convertTrace =
	function( trace )
{
	// input trace
	const t1 = trace.trace;
	// output trace
	let t2 = trace.trace = [ ];

	for( let a = 0, alen = t1.length; a < alen; a++ )
	{
		const t = t1[ a ];

		if( t === 'field' )
		{
			t2.push( '>field', t1[ ++a ] );
			continue;
		}

		if( t === 'item' )
		{
			t2.push( '>item', t1[ ++a ] );
			continue;
		}

		if( t === 'offset' )
		{
			t2.push( '#offset', t1[ ++a ] );
			continue;
		}

		if( t === 'para' )
		{
			t2.push( 'paras', '>para', t1[ ++a ] );
			continue;
		}

		if( t === 'jointPos' )
		{
			t2.push( 'joints', '#joint', t1[ ++a ], 'pos' );
			continue;
		}

		if( t === 'jointShape' )
		{
			t2.push( 'joints', '#joint', t1[ ++a ], 'outline' );
			continue;
		}

		if( t === 'jointOutline' )
		{
			t2.push( 'joints', '#joint', t1[ ++a ], 'outline' );
			continue;
		}

		t2.push( t );
	}

	console.log( );
};
*/


/*
| Converts an object.
*/
/*
def.proto.convertObj =
	function( obj )
{
	if( obj.$type === 'trace' )
	{
		this.convertTrace( obj );
		return;
	}

	const keys = Object.keys( obj );
	for( let key of keys )
	{
		const val = obj[ key ];
		const type = typeof( val );

		if( type === 'array' )
		{
			for( let a = 0, alen = val.length; a < alen; a++ ) this.convertObj( val[ a ] );
		}
		if( type === 'object' ) this.convertObj( val );
	}
};
*/


/*
| Converts a space.
|
| ~spaceName: space name
| ~spaceUid: space uid
*/
/*
def.proto.convertSpace =
	async function( spaceName, spaceUid )
{
	// loads the space
	let seq = 1;
	for( ;; )
	{
		let obj;
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( seq, 'hex' );
		try { obj = await this.sdb.get( dbKey ); }
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}
		obj = JSON.parse( obj );
		this.convertObj( obj );

		if( this.wet ) await this.tdb.put( dbKey, JSON.stringify( obj ) );
		seq++;
	}
};
*/

/*
| Converts a space.
*/
def.proto.convertSpace =
	async function( name, uid )
{
	console.log( '* converting space "' + name + '"' + '(' + uid + ')' );

	// loads the space
	let seq = 1;
	let space = FabricSpace.create( 'items', ItemTwig.create( ) );

	console.log( '** replaying history' );
	for( ;; )
	{
		const dbKey = 'changes:' + uid + ':' + lexi.pack( seq, 'hex' );
		let obj;
		try{ obj = await this.sdb.get( dbKey ); }
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}

		obj = JSON.parse( obj );
		//this.convertObj( obj );
		const changeSkid = ChangeSkid.FromJson( obj, Plan.space );
		if( changeSkid.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = changeSkid.changeTree( space );
	}

	console.log( '** converting portals' );
	{
		let items = space.items;
		for( let item of space.items )
		{
			if( item.timtype !== FabricPortal ) continue;
			item = item.create(
				'refSpace', RefSpace.UsernameTag( item.spaceUser, item.spaceTag ),
				'spaceUser', undefined,
				'spaceTag', undefined,
			);
			items = items.set( item.key, item );
		}
		space = space.create( 'items', items );
	}

	console.log( '** writing as new space' );
	{
		const dbKey = 'changes:' + uid + ':' + lexi.pack( 1, 'hex' );
		const changeSet =
			ChangeSet.create(
				'trace', TraceRoot.root( Plan.space ),
				'val', space,
				'prev', FabricSpace.create( 'items', ItemTwig.create( ) )
			);

		const cw =
			ChangeWrap.create(
				'cid', Uid.newUid( ),
				'changeList', ChangeList.Elements( changeSet ),
				'seq', 1
			);
		const cs = ChangeSkid.FromChangeWrap( cw, ':convert', 1, Date.now( ) );
		if( this.wet ) await this.tdb.put( dbKey, JSON.stringify( cs ) );
	}
};


def.proto.convertSpaces =
	async function( )

{
	console.log( 'converting spaces' );
	const spaceNames = await this.getSpaceNames( );
	for( let spaceName of spaceNames )
	{
		const spaceMeta = await this.sdb.get( spaceName );
		const smJson = JSON.parse( spaceMeta );
		console.log( 'doing', spaceName, '(' +  smJson.uid + ')' );
		if( this.wet ) await this.tdb.put( spaceName, spaceMeta );
		await this.convertSpace( spaceName, smJson.uid );
	}
};


def.proto.convertUsers =
	async function( )
{
	console.log( 'converting users' );
	const names = await this.getUserNames( );
	for( let name of names )
	{
		const dbKey = 'users:' + name;
		const dbVal = await this.sdb.get( dbKey );
		if( this.wet ) await this.tdb.put( dbKey, dbVal );
	}
};


/*
| Returns all space names.
*/
def.proto.getSpaceNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.sdb.createReadStream(
				{ gt: 'spaces:', lt: 'spaces;', values: false }
			)
			.on( 'data', ( data ) => names.push( data ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};


/*
| Returns all user names.
*/
def.proto.getUserNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.sdb.createReadStream( { gt: 'users:', lt: 'users;' } )
			.on( 'data', ( data ) => names.push( JSON.parse( data.value ).name ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};


/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) console.log( '-- WET RUN! --' );
	else console.log( '-- dry run --' );

	{
		console.log( 'loading fonts' );
		const promise = util.promisify( FontRoot.load );
		await promise( 'DejaVuSans-Regular' );
	}

	const sdb = level( this.config.src );
	let srcVersion;
	srcVersion = await sdb.get( 'version' );
	if( srcVersion.notFound )
	{
		console.log( 'source repository not found!' );
		return;
	}
	if( srcVersion !== '' + this.fromVersion ) throw new Error( 'wrong src version' );

	let tdb;
	if( this.wet )
	{
		tdb = level( this.config.trg );
		console.log( 'destroying possible preexisting target' );
		await tdb.clear( );
		console.log( 'establishing target' );
		await tdb.put( 'version', this.toVersion + '' );
	}

	let root = this.create( 'sdb', sdb, 'tdb', tdb );
	await root.convertUsers( );
	await root.convertSpaces( );
	console.log( 'done' );
};


} );
