/*
| Converts a v41 repository to v42.
|
| * Changes "fontsize" to "fontSize".
*/
'use strict';

def.attributes =
{
	// converting from this version
	fromVersion: { type: 'number' },

	// source database
	srcDb: { type: [ 'undefined', 'protean' ] },

	// source dir
	srcDir: { type: [ 'undefined', 'string' ] },

	// converting to this version
	toVersion: { type: 'number' },

	// target database
	trgDb: { type: [ 'undefined', 'protean' ] },

	// source dir
	trgDir: { type: [ 'undefined', 'string' ] },

	// dry or wet operation
	wet: { type: 'boolean' },
};

const level = require( 'level' );
const lexi = require( 'lexicographic-integer' );

const ChangeList = tim.require( 'tim:Change/List' );
const ChangeTreeSet = tim.require( 'tim:Change/Tree/Set' );
const ChangeSkid = tim.require( 'Database/ChangeSkid/Self' );
const ChangeWrap = tim.require( 'tim:Change/Wrap' );
const ItemTwig = tim.require( 'Fabric/Item/Twig' );
const Plan = tim.require( 'Trace/Plan' );
const Space = tim.require( 'Fabric/Space' );
const TraceRoot = tim.require( 'Trace/Root' );
const Uid = tim.require( 'tim:Uid' );
//const FontRoot = tim.require( 'gleam/font/root' );

/*
| Converts an object.
*/
def.proto.convertObj =
	function( obj )
{
	/*
	if( obj.$type === 'twig:Para' )
	{
		obj.$type = 'list:Para';
		const list = obj.list = [ ];
		for( let key of obj.keys )
		{
			list.push( obj.twig[ key ] );
		}
		delete obj.keys;
		delete obj.twig;
		return;
	}
	*/

	if( obj.fontsize )
	{
		obj.fontSize = obj.fontsize;
		delete obj.fontsize;
	}

	const keys = Object.keys( obj );
	for( let key of keys )
	{
		const val = obj[ key ];
		const type = typeof( val );
		switch( type )
		{
			case 'array':
				for( let a = 0, alen = val.length; a < alen; a++ ) this.convertObj( val[ a ] );
				break;
			case 'object':
				this.convertObj( val );
				break;
		}
	}
};

/*
| Converts a space.
|
| ~spaceName: space name
| ~spaceUid: space uid
*/
/*
def.proto.convertSpace =
	async function( spaceName, spaceUid )
{
	// loads the space
	let seq = 1;
	for( ;; )
	{
		let obj;
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( seq, 'hex' );
		try { obj = await this.srcDb.get( dbKey ); }
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}
		obj = JSON.parse( obj );
		this.convertObj( obj );
		if( this.wet ) await this.trgDb.put( dbKey, JSON.stringify( obj ) );
		seq++;
	}
};
*/

/*
| Converts a space.
*/
def.proto.convertSpace =
	async function( spaceName, spaceUid )
{
	console.log( '* converting space "' + spaceName + '"' + '(' + spaceUid + ')' );

	// loads the space
	let seq = 1;
	let space = Space.create( 'items', ItemTwig.create( ) );

	console.log( '** replaying history' );
	for( ;; )
	{
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( seq, 'hex' );
		let obj;
		try{ obj = await this.srcDb.get( dbKey ); }
		catch( e )
		{
			if( e.notFound ) break;
			else throw e;
		}

		obj = JSON.parse( obj );
		//this.convertObj( obj );
		//console.inspect( obj );
		const changeSkid = ChangeSkid.FromJson( obj, Plan.space );
		if( changeSkid.seq !== seq ) throw new Error( 'sequence mismatch' );
		seq++;
		space = changeSkid.changeTree( space );
	}

	console.log( '** converting json' );
	{
		const spj = JSON.parse( space.jsonfy( '' ) );
		this.convertObj( spj );
		space = Space.FromJson( spj, Plan.space );
	}

	console.log( '** writing as new space' );
	{
		const dbKey = 'changes:' + spaceUid + ':' + lexi.pack( 1, 'hex' );
		const changeSet =
			ChangeTreeSet.create(
				'trace', TraceRoot.space,
				'val', space,
				'prev', Space.create( 'items', ItemTwig.create( ) )
			);
		const cw =
			ChangeWrap.create(
				'cid', Uid.newUid( ),
				'changeList', ChangeList.Elements( changeSet ),
				'seq', 1
			);
		const cs = ChangeSkid.FromChangeWrap( cw, ':convert', 1, Date.now( ) );
		if( this.wet ) await this.trgDb.put( dbKey, cs.jsonfy( '' ) );
	}
};

def.proto.convertSpaces =
	async function( )
{
	console.log( 'converting spaces' );
	const spaceNames = await this.getSpaceNames( );
	for( let spaceName of spaceNames )
	{
		const spaceMeta = await this.srcDb.get( spaceName );
		const smJson = JSON.parse( spaceMeta );
		console.log( 'doing', spaceName, '(' +  smJson.uid + ')' );
		if( this.wet ) await this.trgDb.put( spaceName, spaceMeta );
		await this.convertSpace( spaceName, smJson.uid );
	}
};

def.proto.convertUsers =
	async function( )
{
	console.log( 'converting users' );
	const names = await this.getUserNames( );
	for( let name of names )
	{
		const dbKey = 'users:' + name;
		const dbVal = await this.srcDb.get( dbKey );
		if( this.wet ) await this.trgDb.put( dbKey, dbVal );
	}
};

/*
| Returns all space names.
*/
def.proto.getSpaceNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.srcDb.createReadStream(
				{ gt: 'spaces:', lt: 'spaces;', values: false }
			)
			.on( 'data', ( data ) => names.push( data ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};

/*
| Returns all user names.
*/
def.proto.getUserNames =
	async function( )
{
	const names = [ ];
	const p = new Promise(
		( resolve, reject ) => {
			this.srcDb.createReadStream( { gt: 'users:', lt: 'users;' } )
			.on( 'data', ( data ) => names.push( JSON.parse( data.value ).name ) )
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);
	await p;
	return names;
};

/*
| Initializes the converter.
*/
def.static.init =
	function( fromVersion )
{
	if( process.argv.length !== 3 ) { Self.usage( ); return; }
	const arg = process.argv[ 2 ];

	const toVersion = fromVersion + 1;
	const srcDir = '/home/axel/plotle-' + fromVersion;
	const trgDir = '/home/axel/plotle-' + toVersion;

	// if false doesn't do anything to the target.
	let wet;
	switch( arg )
	{
		case 'dry': wet = false; break;
		case 'wet': wet = true; break;
		default: Self.usage( ); return;
	}

	Self.create(
		'fromVersion', fromVersion,
		'srcDir', srcDir,
		'toVersion', toVersion,
		'trgDir', trgDir,
		'wet', wet
	).run( );
};

/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) console.log( '-- WET RUN! --' );
	else console.log( '-- dry run --' );

	//{
	//	console.log( 'loading fonts' );
	//	const promise = util.promisify( FontRoot.load );
	//	await promise( 'DejaVuSans-Regular' );
	//}

	const srcDb = level( this.srcDir );
	const srcVersion = await srcDb.get( 'version' );
	if( srcVersion.notFound )
	{
		console.log( 'source repository not found!' );
		return;
	}
	if( srcVersion !== '' + this.fromVersion ) throw new Error( 'wrong src version' );

	let trgDb;
	if( this.wet )
	{
		trgDb = level( this.trgDir );
		console.log( 'destroying possible preexisting target' );
		await trgDb.clear( );
		console.log( 'establishing target' );
		await trgDb.put( 'version', this.toVersion + '' );
	}

	let root = this.create( 'srcDb', srcDb, 'trgDb', trgDb );
	await root.convertUsers( );
	await root.convertSpaces( );
	console.log( 'done' );
};

/*
| Prints out usage info.
*/
def.static.usage =
	function( )
{
	console.log( 'USAGE: node ' + module.filename + ' [dry or wet]' );
};
