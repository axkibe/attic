/*
| Starts the converter.
*/
Error.stackTraceLimit = Infinity;

const fromVersion = 43;

global.CHECK = true;
global.NODE = true;
global.VISUAL = true;

const cname = 'Convert-' + fromVersion + '-to-' + ( fromVersion + 1 );

await import( 'ti2c' );
await import( 'ti2c-ot' );
await import( 'ti2c-gleam' );

const pkg = await ti2c.register( 'plotle', import.meta, 'src/', cname + '/Start.js' );
const Root = await pkg.import( cname + '/Root' );
await Root.init( fromVersion );
