/*
| Converts a v30 repository to v31.
|
| tim json uses now $type instead of type
*/
'use strict';


tim.define( module, ( def ) => {


if( TIM )
{
	def.attributes =
	{
		config : { type : 'protean' },

		fromVersion : { type : 'number' },

		toVersion : { type : 'number' },

		wet : { type : 'boolean' },
	};
}


const level = require( 'level' );

tim.require( '../../trace/base' );
const log = tim.require( '../../server/log' );


/*
| Converts an object.
*/
const convertObj =
	function( obj )
{
	const keys = Object.keys( obj );
	for( let key of keys )
	{
		const val = obj[ key ];
		const type = typeof( val );

		if( key === 'type' )
		{
			obj.$type = val;
			delete obj.type;
			continue;
		}

		if( type === 'object' ) obj[ key ] = convertObj( val );
		if( type === 'array' )
		{
			for( let a = 0, alen = val.length; a < alen; a++ )
			{
				val[ a ] = this.convertObj( val[ a ] );
			}
		}
	}
	return obj;
};


/*
| The main runner.
*/
def.proto.run =
	async function( )
{
	if( this.wet ) log.log( '-- WET RUN! --' );
	else log.log( '-- dry run --' );

	const sdb = level( this.config.src );
	const tdb = level( this.config.trg );

	if( this.wet )
	{
		log.log( 'destroying possible preexisting target' );
		await tdb.clear( );
	}

	log.log( 'converting all entries' );
	const p = new Promise(
		( resolve, reject ) => {
			sdb.createReadStream()
			.on( 'data',
				( data ) =>
				{
					if( data.key === 'version' )
					{
						tdb.put( 'version', '31' );
						return;
					}

					console.log( data.key, '=', data.value );
					let json = JSON.parse( data.value );
					json = convertObj( json );
					tdb.put( data.key, JSON.stringify( json ) );
				}
			)
			.on( 'error', reject )
			.on( 'close', resolve );
		}
	);

	await p;
	log.log( 'done' );
};


} );
